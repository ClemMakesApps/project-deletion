# Project Deletion

This node script will delete projects from a group's subgroups that have not seen any activity in X days (X is configurable and defaults to 365)

## Program Parameters

To view the program parameters, run node index.js --help

## Running this program (Locally)

1. yarn install
1. node index.js --token "token" --root-group-id "group-id"

## Running this program (Scheduled Pipeline)

1. Go to the pipelines schedule page for the project
1. Click the New schedule button
1. Add a description and interval pattern
1. Input the variable key and variable value that you would pass into the program (as though you would if running locally). For example, add the variable key TOKEN with the variable value of your GitLab private token
