const axios = require('axios');

function deleteProject({ apiUrl, token}, projectId) {
  return axios.delete(`${apiUrl}/projects/${projectId}`, {
    headers: {
      'PRIVATE-TOKEN': token,
    },
  });
}

module.exports = {
  deleteProject
};
