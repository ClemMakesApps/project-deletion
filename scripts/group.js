const axios = require('axios');

function getDescendantGroups({ apiUrl, token}, groupId, skipGroups = [], perPage = 100) {
  return axios.get(`${apiUrl}/groups/${groupId}/descendant_groups?per_page=${perPage}&skip_groups[]=${skipGroups}`, {
    headers: {
      'PRIVATE-TOKEN': token,
    },
  });
}

function getProjectsFromGroup({ apiUrl, token}, groupId, perPage = 100) {
  return axios.get(`${apiUrl}/groups/${groupId}/projects?per_page=${perPage}`, {
    headers: {
      'PRIVATE-TOKEN': token,
    },
  });
}

module.exports = {
  getDescendantGroups,
  getProjectsFromGroup
};
