const chalk = require('chalk');

const error = chalk.bold.red;
const info = chalk.blueBright;

const logError = (message) => console.log(error(message));
const logInfo = (message) => console.log(info(message));
const log = (message) => console.log(message);

module.exports = {
  logError,
  logInfo,
  log,
}
