const path = require('path');
const program = require('commander');

const { logError, logInfo } = require('./scripts/log');

const groupApi = require('./scripts/group');
const projectApi = require('./scripts/project');

const API_URL = 'https://gitlab.com/api/v4';

program
  .version('1.0.0')
  .option('-t, --token <token>', 'GitLab private token (Required)')
  .option('-rgi, --root-group-id <group-id>', 'Root group ID of gitlab group (Required)')
  .option('-dt, --days-threshold <days>', 'How many days old should the project be before deleting (Optional)')
  .option('-sg, --skip-groups <group-ids...>', 'Group IDs to skip over for the deletion (Optional)')
  .parse(process.argv);

const TOKEN = program.token;
const ROOT_GROUP_ID = program.rootGroupId;
const DAYS_THRESHOLD = program.daysThreshold || 365;
const SKIP_GROUPS = program.skipGroups

const apiConfig = { apiUrl: API_URL, token: TOKEN };

groupApi.getDescendantGroups(apiConfig, ROOT_GROUP_ID, SKIP_GROUPS)
  .then(({ data }) => {
    const getProjects = data.map(group => groupApi.getProjectsFromGroup(apiConfig, group.id));
    return Promise.all(getProjects);
  })
  .then((response) => {
    const data = response.map(({ data }) => data);
    const projects = data.flat()

    projects.forEach((project) => {
      const lastActivity = new Date(project.last_activity_at);

      const threshold = new Date();
      threshold.setDate(threshold.getDate()-DAYS_THRESHOLD);

      const shouldDelete = lastActivity < threshold;

      if (shouldDelete) {
        logInfo(`Deleting project ${project.web_url}`)
        projectApi.deleteProject(apiConfig, project.id)
      }
    })
  })
  .catch(error => logError(error));
